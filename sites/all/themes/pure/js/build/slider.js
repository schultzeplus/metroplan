(function ($) {
  $(document).ready(function(){
    var slider_el = '.slideshow';
    var slider, // Global slider value to force playing and pausing by direct access of the slider control
      canSlide = true; // Global switch to monitor video state

    // Load the YouTube API. For some reason it's required to load it like this
    var tag = document.createElement('script');
      tag.src = "//www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      // Setup a callback for the YouTube api to attach video event handlers
    window.onYouTubeIframeAPIReady = function(){
      // Iterate through all videos
      $(slider_el + ' iframe').each(function(){
        // Create a new player pointer; "this" is a DOMElement of the player's iframe
        var player = new YT.Player(this, {
          playerVars: {
            autoplay: 0
          }
        });

        // Watch for changes on the player
        player.addEventListener("onStateChange", function(state){
          switch(state.data)
          {
            // If the user is playing a video, stop the slider
            case YT.PlayerState.PLAYING:
              slider.flexslider("stop");
              canSlide = false;
              break;
            // The video is no longer player, give the go-ahead to start the slider back up
            case YT.PlayerState.ENDED:
            case YT.PlayerState.PAUSED:
              slider.flexslider("play");
              slider.flexslider("next");
              canSlide = true;
              break;
          }
        });

        $(this).data('player', player);
      });
    }

    // Setup the slider control
    var dir_nav = ($(slider_el).hasClass('dir-nav') == true) ? true : false;
    slider = $(slider_el)
      .flexslider({
        animation: "slide",
        easing: "swing",
        slideshowSpeed: 5000,
        animationSpeed: 900,
        pauseOnHover: true,
        pauseOnAction: true,
        touch: true,
        video: true,
        controlNav: true,
        directionNav: dir_nav,
        prevText: "Previous",
        nextText: "Next",
        animationLoop: true,
        slideshow: true,
        useCSS: false,
          start: function(flexslider){
            _slide = flexslider.slides.eq(flexslider.currentSlide).find(' iframe');
            if(_slide.length>0){
              _slide.data('player').playVideo();
            }
          },
          // Before you go to change slides, make sure you can!
          before: function(){
            if(!canSlide)
              slider.flexslider("stop");
          },
          after: function(flexslider){
            _slide = flexslider.slides.eq(flexslider.currentSlide).find(' iframe');
            if(_slide.length>0){
              _slide.data('player').playVideo();
            }
          },
    });

    slider.on("load", "click", ".flex-prev, .flex-next", function(){
      canSlide = true;
      $(slider_el + ' iframe').each(function(){
        $(this).data('player').pauseVideo();
      });
    });

    $('.customer-slider').flexslider({
      animation: 'slide',
      controlNav: false,
      directionNav: true,
    });
  });
}(jQuery));
