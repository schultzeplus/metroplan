(function($) {
    $('.accordion > dt > h3').click(function() {
        $this = $(this);
        $target =  $this.closest(".accordion");
        $body = $target.find("dd");

        if(!$target.hasClass('active')){
            if($(".accordion.active").length) {
                $(".accordion.active").removeClass('active').find("dd").slideUp();;
            }
            $target.addClass('active');
            $body.slideDown();
        } else {
            $target.removeClass('active');
            $body.slideUp();
        }

        return false;
    });
})(jQuery);