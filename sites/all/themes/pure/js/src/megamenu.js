(function ($) {
    $(document).ready(function() {
        var navOpen = false;

        $bg = $("#navBg");

        $(".menu>h2").click(function() {
            $("h2.open").removeClass("open");
            $(this).addClass("open");
            var $el = $(this).parent().find(".region");
            var $mel = $(this).parent();
            if(window.outerWidth > 1080) {
                $el.show();
                if (navOpen) {
                    $oelem = $("#header div.open");
                    $oelem.removeClass("open");
                    setTimeout(function () {
                        $oelem.removeClass("quick");
                    }, 1);
                    $el.addClass("quick open");
                } else {
                    $bg.show();
                    $el.addClass("open");
                    navOpen = true;
                }

            } else {
                if(!$mel.hasClass("regopen")) {
                    if ($(".menu.regopen").length) {
                        $(".menu.regopen").removeClass("regopen").find(".region").slideUp();
                    }
                    $mel.addClass("regopen").find(".region").slideDown();
                }
            }
        });

        $bg.click(close);

        $(".close").click(close);

        function close() {
            $bg.hide();
            navOpen = false;
            $("#header div.open").removeClass("quick").removeClass("open");
            $("h2.open").removeClass("open");
        }
    });


    $microman = $("#microman");
    $nav = $("#navi");

    $microman.click(function() {
        $nav.slideToggle();
    });

    var $secondLayer = $(".region ul li:has(ul)");

    $secondLayer.click(function() {
        if(window.outerWidth < 1080) {
            $el = $(this);
            $thirdLayer = $el.children("ul");
            if (!$el.hasClass("accopen")) {
                if ($("li.accopen").length) {
                    $("li.accopen").removeClass("accopen").find("ul").slideUp();
                }
                $el.addClass("accopen").find("ul").slideDown();
            }
        }
    });

    $secondLayer.children("a").click(function(event){
        if(window.outerWidth < 1080) {
            event.preventDefault();
        }
    });

    $(window).resize(function() {
        if(window.outerWidth > 1080) {
            $(".menu.regopen").removeClass("regopen");
            $("li.accopen").removeClass("accopen");
        }
    });
}(jQuery));