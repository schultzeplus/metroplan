(function($) {
    var activeLogo = false;
    function changeLogo(bool) {
        var logo = $("#block-pure-branding img");

        if(bool !== activeLogo) {
            if (bool) {
                logo.addClass("m").attr("src", "/sites/all/themes/pure/img/metroplan_m.svg");
            } else {
                logo.removeClass("m").attr("src", "/sites/all/themes/pure/img/metroplan_grau.svg");
            }

            activeLogo = bool;
        }
    }

    $(window).scroll(function() {
       if($(this).width() >= 500) {
           if($(this).scrollTop() >= 500) {
               changeLogo(true);
           } else {
               changeLogo(false);
           }
       }
    });

    function scrollToJob() {
        if ($(".jobs-overview").length && window.location.hash.length > 3) {
            var target = $(window.location.hash);
            if ($(".accordion.active").length) {
                $(".accordion.active").removeClass("active").find("dd").hide();
            }
            target.addClass("active").find("dd").slideDown();

            $('html, body').animate({
                scrollTop: target.offset().top - $("header").height()
            }, 1000);
        }
    }

    scrollToJob();

    $(window).bind("hashchange", scrollToJob);

})(jQuery);
