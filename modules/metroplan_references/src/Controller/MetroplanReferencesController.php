<?php

namespace Drupal\metroplan_references\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\taxonomy\TermInterface;

class MetroplanReferencesController extends ControllerBase{

  public function index($term_name, Request $request) {
    $term_name = str_replace('-', ' ', $term_name);
    $terms = taxonomy_term_load_multiple_by_name($term_name);

    $nodes = [];

    foreach ($terms as $term) {
      $query = \Drupal::entityQuery('node')
        ->condition('status', 1)
        ->condition('field_industry.entity.tid', $term->Id());

      $nids= $query->execute();
      if($nids) {
        $nodesArray = \Drupal\node\Entity\Node::loadMultiple($nids);
        $nodes = array_merge($nodes, $nodesArray);
      }
    }

    $term_name = $term_name;

    return [
      '#type' => 'markup',
      '#markup' => 'Test works!',
    ];
  }

}