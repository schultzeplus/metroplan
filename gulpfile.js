var gulp = require("gulp");

var sass = require("gulp-sass");
var autopre = require("gulp-autoprefixer");
var cssmin = require("gulp-cssmin");
var uglify = require("gulp-uglify");

gulp.task('styles', function() {
    gulp.src('./sites/all/themes/pure/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autopre())
        .pipe(cssmin())
        .pipe(gulp.dest('./sites/all/themes/pure/css/'))
});

gulp.task('scripts', function() {
    return gulp.src('./sites/all/themes/pure/js/src/**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('./sites/all/themes/pure/js/build'));
});

//Watch task
gulp.task('watch',function() {
    gulp.watch('./sites/all/themes/pure/scss/**/*.scss',['styles']);
    gulp.watch('./sites/all/themes/pure/js/src/**/*.js',['scripts'])
});
